use actix_web::{App, server};
use listenfd::ListenFd;


type GetApp = fn() -> Vec<App>;

pub fn get_server(get_apps: GetApp) -> server::HttpServer<Vec<App>, GetApp> {
    let mut listenfd = ListenFd::from_env();
    let server = server::new(get_apps);
    server.bind("0.0.0.0:8088").unwrap()
}
