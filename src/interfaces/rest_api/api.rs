use actix_web::App;

use super::resources::Resource;


pub struct RestApi {
    resources: Vec<Resource>,
}


impl RestApi {
    pub fn new() -> RestApi {
        RestApi {
            resources: Vec::new(),
        }
    }

    pub fn add_resource(&mut self, resource: Resource) {
        self.resources.push(resource);
    }

    pub fn generate_app(&mut self) -> App {
        let mut app = App::new().prefix("/api");
        for resource in self.resources.iter_mut() {
            app = resource.register(app);
        }
        return app;
    }
}
