use actix_web::{App, HttpRequest, HttpResponse};
use actix_web::http::Method;

use crate::use_cases::FunctionUseCase;
use super::views::function_use_case_view;


struct Route {
    handler: fn(HttpRequest) -> HttpResponse,
    method: Method,
    path: String,
}


pub struct Resource {
    routes: Vec<Route>,
    list_path: String,
    detail_path: String,
}


impl Resource {
    pub fn new(name: &str) -> Resource {
        let list_path = format!("/{}", name);
        let detail_path = format!("{}/<uid>", list_path);
        Resource {
            routes: Vec::new(),
            list_path: list_path,
            detail_path: detail_path,
        }
    }

    pub fn register(&mut self, mut app: App) -> App {
        while let Some(Route {handler, method, path}) = self.routes.pop() {
            app = app.route(&path, method, handler);
        }
        return app;
    }

    pub fn add_list_use_case<UseCase: FunctionUseCase>(&mut self) {
        self.routes.push(Route {
            handler: function_use_case_view::<UseCase>,
            method: Method::GET,
            path: self.list_path.clone(),
        });
    }
}
