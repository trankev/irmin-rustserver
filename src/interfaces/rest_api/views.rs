use actix_web::{HttpRequest, HttpResponse};
use actix_web::http::StatusCode;

use crate::use_cases::FunctionUseCase;


pub fn function_use_case_view<UseCase>(_request: HttpRequest) -> HttpResponse where
    UseCase: FunctionUseCase,
{
    let response = UseCase::handle();
    let status_code = if response.errors.len() > 0 { StatusCode::BAD_REQUEST } else { StatusCode::OK };
    HttpResponse::build(status_code)
        .json(response)
}
