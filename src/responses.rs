use serde::Serialize;


pub trait DataTypeTrait: Serialize {}
pub trait MetaDataTypeTrait: Serialize {}


#[derive(Serialize)]
pub struct Error {
    code: String,
    title: String,
    source: Option<String>,
}


#[derive(Serialize)]
pub struct Response<DataType, MetaDataType> where
    DataType: DataTypeTrait,
    MetaDataType: MetaDataTypeTrait,
{
    data: Option<DataType>,
    metadata: Option<MetaDataType>,
    pub errors: Vec<Error>,
}


impl<DataType, MetaDataType> Response<DataType, MetaDataType> where
    DataType: DataTypeTrait,
    MetaDataType: MetaDataTypeTrait,
{
    pub fn success(data: DataType, metadata: Option<MetaDataType>) -> Response<DataType, MetaDataType> {
        Response {
            data: Some(data),
            metadata: metadata,
            errors: Vec::new(),
        }
    }

    pub fn error(errors: Vec<Error>) -> Response<DataType, MetaDataType> {
        Response {
            data: None,
            metadata: None,
            errors: errors,
        }
    }
}
