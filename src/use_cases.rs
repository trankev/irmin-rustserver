use super::responses::{DataTypeTrait, MetaDataTypeTrait, Response};


pub trait FunctionUseCase {
    type DataType: DataTypeTrait;
    type MetaDataType: MetaDataTypeTrait;

    fn handle() -> Response<Self::DataType, Self::MetaDataType>;
}
